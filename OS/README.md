# OS

## Super quiz!

### Instruction
Vous devez répondre au questionnaire directement dans ce README en dessous de chaque question. Ces questions touchent plusieurs aspects de la programmation de bas niveau et les réponses sont généralement assez courtes.

###### Question 1
Quelle est la différence entre le heap et le stack (le monceau et la pile).

La heap est dans la RAM avec les malloc et les new, tandis que la stack est plus "core" que ça et contient les calls récursifs de fonctions et variables non-allouées avec un new à chaque scope d'une fonction à l'autre. Par exemple, un objet fait avec new en C++ ira dans la heap, mais son pointeur sera dans la stack évidemment.

###### Question 2
Que fais la fonction malloc?

C'est une fonction pour manuellement demander de la mémoire dans la heap. Il faut généralement spécifier combien qu'on en demande. La fonction sizeof() est bien utile parfois pour savoir combien allouer de mémoire pour ce type de variable donné.


###### Question 3
Décrivez le fonctionnement de ce code.

```
uint16_t lastKeys = 0;
uint16_t diff, currentKeys;

while (1) {
	currentKeys = scanKeyboard();
	if ((diff = (uint16_t) (currentKeys & (~lastKeys))) || !currentKeys)
		lastKeys = currentKeys;
	faire_quelque_chose(diff);
}
```
1. Le symbole `&` est bitwise, tandis que `&&` ne l'est pas. Ainsi, sur les uint16_t, on a des entiers positifs sur 16 bits. l'opérateur `~` inverse (bitwise) les bits de lastKeys. Donc on vérifie que currentKeys est différent en au moins un bit de lastKeys. On veut entrer dans le if avec un or `||` si currentKeys est égal à 0. currentKeys n'est pas initialisé, ce qui est dangereux.
2. On set `lastKeys = currentKeys`. En gros, on set ça quand on a scanné quelque chose de différent de ce qu'on avait, ou si le scan est de 0.
3. une fonction mystérieuse `faire_quelque_chose(diff);` peut changer les variables en side effect sans qu'on le sache.
4. Pour résumer, on boucle sur la fonction `faire_quelque_chose(diff);`, et on change nos variables à chaque fois que ce qu'il y a dans le clavier change ou est nul. Ça ressemble presqu'à un buffer de keyboard, mais c'est vraiment pas clean si vraiment c'est ça.


###### Question 4
Que signifie les 8 paramètres de cette commande GCC:  
`gcc -Os -Wall -g3 -flto -lpthread -o main main.c`
- Os: ça sert à optimizer, mais en n'augmentant pas l'espace que prendrait l'éxécutable. Pour être plus précis, ça active toutes les optimizations que `-O2` aurait fait mais qui n'augmentent pas la taille de l'éxécutable.

- Wall: sert à afficher les messages de warnings. En pratique, ça veut dire "Warn all", donc à activer pour avoir du code propre. Dans certains cas cependant, c'est difficile de pas avoir de warnings pour quelques applications rares.

- g3:

- flto: Une optimization au linker, un peu comme avec `-O`. En général, c'est pour merger le GIMPLE, un représentation interne de GCC.

- lpthread: on demande de linker pthread, une librairie pour faire du multithreading et utile pour des trucs ŗeliés à ça. L'utilisation de Boost demande parfois le header pthread.h. Les threads boost sont construites par dessus pthread.

- o main main.c: On demande à produire l'output `-o` de `main.c` vers le fichier éxécutable `main`.


###### Question 5
À quoi sert le DMA dans un microcontrolleur ou un ordinateur.

- Le Direct Memory Access sert à accéder à de la mémoire externe au CPU, par exemple notamment dans les cartes graphiques. Du code C ou C++ peut utiliser le DMA afin d'envoyer certains programes, instructions ou données vers le GPU pour y faire des calculs. Ça passe sur les BUS PCI par exemple dans les cas des cartes graphiques internes. Dans un microcontrolleur, c'est la même histoire: on voudrait accéder à de la mémoire externe, par exemple, une carte quantique de D-Wave. Le DMA permet au CPU de faire autre chose pendant que la mémoire est transférée, plutôt que de bloquer sur le transfert. Sans DMA, accéder à cette mémoire est aussi possible alors.


###### Question 6
Quelle est la différence entre mutex et sémaphore?

- Les Mutex servent à faire des locks sur des objets. Par exemple, on veut locker une variable pour la lire et l'écrire lorsque l'opération n'est pas atomique et pourrait causer des `race conditions`.
- Les Sémophores servent en tant que mécanismes d'envoi de signal. Par exemple, ça peut prendre la forme d'un compteur à 1 bit (zéro ou un) lequel indique si un service est disponible ou non. Par exemple, le sémaphore est intéressant dans le pattern producer-consumer.
- Un mutex peut être délocké seulement par le thread qui l'a locké, tandis qu'un sémaphore peut être changé par plusieurs threads.

###### Question 7
Un microcontrolleur lit, avec son convertisseur analogique à numérique à 12 bits, la température d'une cuve de fermentation à l'aide d'un thermomètre. Votre microcontrolleur fonctionne en 5V. Le thermomètre retourne une valeur entre 0 et 5V correspondant aux températures respectivement entre 15-25 degrées Celcius. Assumez que le comportement du thermomètre est linéaire.
Donnez la précision de lecture de votre microcontrolleur. Dans quel type de variable devriez-vous sauvegarder la valeur lue de façon à conserver le maximum de précision (char, int, long, float, double, etc), pourquoi? S'il y a lieu, expliquez aussi à quoi correspondent les valeurs enregistrées. (Max 10 lignes)

- On a 12 bits, donc `2^12 - 1 = 4095` chiffres discrets pour la lecture.
- La précision de lecture est égale à `(25 - 15)/(2^12 - 1) = 0.002442002 degrés Celcius`.
- On veut une variable non-signée d'au moins 12 bits ça peut se faire avec: `struct bitset12 { unsigned val:12; };`
- Pour les types proposés, je prendrais un `int` pour y déposer les bits directement, comme un int ne devrait pas être de moins de 32 bits normalement, et que le fait que c'est signé enlève un bit de valeurs possibles. Personnelement, j'aurais pris un `unsigned short` lequel a seulement 16 bits, mais ce n'est pas dans les choix.



###### Question 8
Expliquer une façon de faire le "debounce" d'un bouton.

- Pour être honnete, pour cette question je connais pas déjà ça. Mais ça semble être relié à la détection d'un keypress pour s'assurer qu'on envoie la commande une fois plutôt que plusieurs fois si le signal flicker.


###### Question 9
Comment pourriez-vous faire pour mesurer la capacitance (en Farrad) d'un condensateur à l'aide d'un Arduino ?


###### Question 10
Que fait ce code et à quoi peut-il servir ? Existe-t-il un équivalent dans la librairie standard du C?

`while (*p++ = *q++) ;`

- On peut réécrire: `while (*(p++) = *(q++)) ;`. Ainsi, p et q sont des pointeurs de tableaux. On les incrémentent et puis on envoie l'addresse de q dans p.

###### Question 11
Décrivez les principales fonctions d'un microprocesseur.

- C'est comme le cerveau de l'ordinateur. Ça sert à faire les opérations mathématiques nécessaires pour faire rouler des programes, donc les opérations nécessaires à ce qu'une machine de turing peut faire. En ce moment, l'architecture de Von Neumann est la plus utilisée. Ainsi, on peut demander à lire des adresses, à les écrires, et à faire des opérations de base sur celles-ci.

- C'est comparable à la récente découverte des NTM et du DNC en deep learning, des réseaux de neurones pouvant apprendre des algorithmes, soit des réseaux de neurones implémentant les concepts clés de l'architecture Von Neumann:
  - https://arxiv.org/abs/1410.5401
  - https://deepmind.com/blog/differentiable-neural-computers/
