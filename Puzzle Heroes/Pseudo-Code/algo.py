# Python3
# Note: this file can be run with `python3 algo.py` and is located in this directory.
# The "count_x[char] = count_x[char] + 1" lines are the chosen barometer operation.

import string

def algo(string_1, string_2):
    # # The two lines of code below are BONUS to the algo and should be ignored for now:
    # if (len(string_1) != len(string_2)):
    #     return False

    # Constant O(1) in memory, the count of each letter for the 2 words.
    char_to_idx = dict(zip(string.ascii_lowercase, range(26)))
    count_1 = [0] * 26
    count_2 = [0] * 26

    # O(n) in time: traversal of both words and read/write once to the constant memory.
    for char in string_1:
        char = char_to_idx[char]
        count_1[char] = count_1[char] + 1
    for char in string_2:
        char = char_to_idx[char]
        count_2[char] = count_2[char] + 1

    # Return condition
    if count_1 == count_2:
        return True
    return False

if __name__ == "__main__":
    # Input to algo:
    string_1 = input().lower()
    string_2 = input().lower()

    print(algo(string_1, string_2))
