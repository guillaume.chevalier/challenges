# Puzzle Heroes

## Deux mots

On veut trouver l'algorithme le plus performant pour le problème qui suit. On vous passe deux mots contenant les caractères a à z en minuscule. On cherche à savoir si les deux mots passés contiennent exactement le même nombre de lettres et avec la même fréquence.

exemple: lorsque l'algorithme reçoit:

* `"allo"` et `"lola"`, retourne `true`
* `"allo"` et `"aaaallllloooo"`, retourne `false`
* `"allo"` et `"polo"`, retourne `false`

### Consignes

* L'algorithme doit s'exécuter en O(n) où n = nombre de lettres du mot 1 + nombre de lettres du mot 2
* L'espace mémoire utilisée doit être en O(1)

### Astuces
* Que se passe-t-il lorsqu'on passe un mot qui contient deux fois la même lettre à votre algo?
* Quels sont les temps d'exécution des opérations sur vos structures de données utilisées?
* Plusieurs boucles consécutives qui ont un temps d'exécution linéaire donne un algo ayant un temps d'exécution linéaire.

Rappel: La règle du maximum

```
Si f1(n) ∈ Θ(g1(n))
et f2(n) ∈ Θ(g2(n)),

alors

f1(n) + f2(n) ∈ Θ(max(g1(n), g2(n))).

Exemple: n + nlog(n) ∈ Θ(n log n)
```

### Réponse
Vous devez écrire votre pseudo-code dans ce README. Vous pouvez écrire votre démarche textuellement pour expliquer votre code et comment fait-il pour s'exécuter en O(n) et avoir O(1) en mémoire.

```
# Python3
# Note: this file can be run with `python3 algo.py` and is located in this directory.
# The "count_x[char] = count_x[char] + 1" lines are the chosen barometer operation.

import string

def algo(string_1, string_2):
    # # The two lines of code below are BONUS to the algo and should be ignored for now:
    # if (len(string_1) != len(string_2)):
    #     return False

    # Constant O(1) in memory, the count of each letter for the 2 words.
    char_to_idx = dict(zip(string.ascii_lowercase, range(26)))
    count_1 = [0] * 26
    count_2 = [0] * 26

    # O(n) in time: traversal of both words and read/write once to the constant memory.
    for char in string_1:
        char = char_to_idx[char]
        count_1[char] = count_1[char] + 1
    for char in string_2:
        char = char_to_idx[char]
        count_2[char] = count_2[char] + 1

    # Return condition
    if count_1 == count_2:
        return True
    return False

if __name__ == "__main__":
    # Input to algo:
    string_1 = input().lower()
    string_2 = input().lower()

    print(algo(string_1, string_2))

```
