# Puzzle Heroes

## Wikiwars
Dans ce défi, vous devez trouver un chemin entre deux pages. Il faut ensuite que vous donniez la liste des pages parcourues. Le défi est considéré comme réussi, si un chemin est donné et valide pour chaque pages demandées.

### Chemins demandés

Computer science → Willy Wonka & the Chocolate Factory
- Computer science
- Stepped Reckoner
- Munich
- Willy Wonka & the Chocolate Factory

Matt Groening  → Brigitte Bardot
- Matt Groening
- The Simpsons
- T-shirt
- Brigitte Bardot

Tim Berners-Lee → Bob Ross
- Tim Berners-Lee
- Twitter
- Shorty Awards
- Bob Ross

James Gosling → Damn Daniel
- James Gosling
- Carnegie Mellon University
- Emoticon
- List of viral videos
- Damn Daniel

Széchenyi thermal bath → Joé Juneau
- Széchenyi thermal bath
- Neo-baroque
- Belfast
- Boston Bruins
- Joé Juneau

Time travel → Satoshi Nakamoto
- Time travel
- Cryonics
- Hal Finney (computer_scientist)
- Satoshi Nakamoto


Vous devez donner le nom des pages parcourues sous chacun des chemins.
