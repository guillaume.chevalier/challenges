# Puzzle Heroes

## Face Mash

Pour chaque photo dans ce répertoire, vous devez trouver quels sont les personnages de la culture populaire qui ont été mélangés pour donner ces magnifiques portraits.
* Il y a toujours DEUX personnages par photo.
* Pour avoir le point vous pouvez inscrire jusqu'à trois noms de personnages pour essayer de tomber sur les deux bons. Vous avez donc droit à une erreur par photo.

Écrire les réponses dans ce README!

#### Réponses:

###### Photo 1
![](photo 1.png)

votre réponse:
1. -
2. -

###### Photo 2
![](photo 2.png)

votre réponse:
1. Neil deGrasse Tyson
2. ANKO MITARASHI FROM NARUTO

###### Photo 3
![](photo 3.png)

votre réponse:

###### Photo 4
![](photo 4.png)

votre réponse:
1. Ned flanders
2. -

###### Photo 5
![](photo 5.png)

votre réponse:

###### Photo 6
![](photo 6.png)

votre réponse:
1. Wilson (le ballon dans le film du gars sur une ile perdue suite à un crash d'avion).
2. -

###### Photo 7
![](photo 7.png)

votre réponse:

###### Photo 8
![](photo 8.png)

votre réponse:
1. Fox (Nintendo)
2. -

###### Photo 9
![](photo 9.png)

votre réponse:
1. Toad (Nintendo)
2. -

###### Photo 10
![](photo 10.png)

votre réponse:
