
import shutil
import inspect


def shield_decorator(some_function):
    # Inspired from: https://realpython.com/blog/python/primer-on-python-decorators/
    def wrapper(**k):
        if open('stickman.txt').read() != open('default.txt').read():
            shutil.copyfile('both.txt',  'stickman.txt')
        else:
            shutil.copyfile('shield.txt',  'stickman.txt')
        ret = some_function()
        shutil.copyfile('default.txt',  'stickman.txt')
        return ret
    return wrapper

def sword_decorator(some_function):
    # Inspired from: https://realpython.com/blog/python/primer-on-python-decorators/
    def wrapper(**k):
        if open('stickman.txt').read() != open('default.txt').read():
            shutil.copyfile('both.txt',  'stickman.txt')
        else:
            shutil.copyfile('sword.txt',  'stickman.txt')
        ret = some_function()
        shutil.copyfile('default.txt',  'stickman.txt')
        return ret
    return wrapper

@sword_decorator
@shield_decorator
def stickman():
    return open('stickman.txt').read()

if __name__ == "__main__":
    print(stickman())
