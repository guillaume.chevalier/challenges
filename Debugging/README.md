# Debugging

## Pacman Map Generator

### Instructions
Pour réussir ce défi, vous devrez debugger le fichier Map.py pour que le programme ait le comportement voulu. Celui-ci devrait tout simplement être en mesure de créer une carte de pacman.

Pour rouler le programme : `python Game.py`

Un bon fonctionnement du programme devrait faire afficher la carte dans une fenêtre qui reste ouverte de la façon suivante :

![](Goal.PNG)

RÉPONSE:
![](MyOutput.png)
